import { Component } from '@angular/core';

import { ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth.service';
import { TabsService } from './core/tabs.service';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import interactionPlugin from '@fullcalendar/interaction';
import esLocale from '@fullcalendar/core/locales/fa';
import { MoodloginComponent } from './moodlogin/moodlogin.component';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    public tabs: TabsService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private modalCtrl:ModalController,
    private router:Router,
    private menu: MenuController
  ) {
    this.initializeApp();
    this.authService.autoLogin();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  calendarOptions: CalendarOptions = {
    // initialView: 'dayGridMonth',
    // dateClick: this.handleDateClick.bind(this), // bind is important!
    // events: [
    //   { title: 'event 1', date: '2019-04-01' },
    //   { title: 'event 2', date: '2019-04-02' }
    // ]
  };

  async moodlogin(){
    this.menu.close();
    const modal = await this.modalCtrl.create({
      component:MoodloginComponent,
      componentProps: { 
        amir :{
          name:'amir',
          path:'https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_Z5vR0tk6pUuROiHXtkoGl84zj0M86XPhrZiwT4lK.mp4'
        } 
      }
    });

    await modal.present();
   
    
  }

  aboutus(){
    this.menu.close();
    this.router.navigate(['/about-us'])
    
  }

  blog(){
    this.menu.close();
    this.router.navigate(['/blog'])
    
  }

  support(){
    this.menu.close();
    this.router.navigate(['/support'])
    
  }
  
  exit(){
    this.menu.close();
    this.router.navigate(['/signin'])
    
  }

  close(){
    this.menu.close();
    
    
  }
}
