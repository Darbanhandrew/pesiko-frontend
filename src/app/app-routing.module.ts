import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
  // { path: '', redirectTo: '/tab3', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('./tab3/tab3.module').then(m => m.Tab3PageModule)
  },
  // {
  //   path: 'tab3',
  //   loadChildren: () => import('./tab3/tab3.module').then(m => m.Tab3PageModule)
  // },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'tab3',
    loadChildren: () => import('./tab3/tab3.module').then(m => m.Tab3PageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then(m => m.Tab2PageModule)
  },
  {
    path: 'conditions-list',
    loadChildren: () => import('./conditions-list/conditions-list.module').then( m => m.ConditionsListPageModule)
  },
  {
    path: 'signin',
    loadChildren: () => import('./signin/signin.module').then( m => m.SigninPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'forget-pass',
    loadChildren: () => import('./forget-pass/forget-pass.module').then( m => m.ForgetPassPageModule)
  },
  {
    path: 'reset-pass',
    loadChildren: () => import('./reset-pass/reset-pass.module').then( m => m.ResetPassPageModule)
  },
  {
    path: 'verification',
    loadChildren: () => import('./verification/verification.module').then( m => m.VerificationPageModule)
  },
  {
    path: 'therapy',
    loadChildren: () => import('./therapy/therapy.module').then( m => m.TherapyPageModule)
  },
  {
    path: 'self-con',
    loadChildren: () => import('./self-con/self-con.module').then( m => m.SelfConPageModule)
  },
  {
    path: 'course',
    loadChildren: () => import('./course/course.module').then( m => m.CoursePageModule)
  },
  
  // { 
  //   path: '',
  //   redirectTo: '/tab3',
  //   pathMatch: 'full'
  // },
  {
    path: 'therapy-gen',
    loadChildren: () => import('./therapy-gen/therapy-gen.module').then( m => m.TherapyGenPageModule)
  },
  {
    path: 'course',
    loadChildren: () => import('./course/course.module').then( m => m.CoursePageModule)
  },
  {
    path: 'podcast-page',
    loadChildren: () => import('./podcast-page/podcast-page.module').then( m => m.PodcastPagePageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'reserve',
    loadChildren: () => import('./reserve/reserve.module').then( m => m.ReservePageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./blog/blog.module').then( m => m.BlogPageModule)
  },
  {
    path: 'test-page',
    loadChildren: () => import('./test-page/test-page.module').then( m => m.TestPagePageModule)
  },
  {
    path: 'course-play',
    loadChildren: () => import('./course-play/course-play.module').then( m => m.CoursePlayPageModule)
  },
  {
    path: 'meditation',
    loadChildren: () => import('./meditation/meditation.module').then( m => m.MeditationPageModule)
  },
  {
    path: 'sleep-podcast',
    loadChildren: () => import('./sleep-podcast/sleep-podcast.module').then( m => m.SleepPodcastPageModule)
  },
  {
    path: 'sleep-disorder',
    loadChildren: () => import('./sleep-disorder/sleep-disorder.module').then( m => m.SleepDisorderPageModule)
  },
  {
    path: 'time-reserve',
    loadChildren: () => import('./time-reserve/time-reserve.module').then( m => m.TimeReservePageModule)
  },
  {
    path: 'user-log',
    loadChildren: () => import('./user-log/user-log.module').then( m => m.UserLogPageModule)
  },
  {
    path: 'credit',
    loadChildren: () => import('./credit/credit.module').then( m => m.CreditPageModule)
  },
  {
    path: 'favs',
    loadChildren: () => import('./favs/favs.module').then( m => m.FavsPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'payment-res',
    loadChildren: () => import('./payment-res/payment-res.module').then( m => m.PaymentResPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
