import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserLogPageRoutingModule } from './user-log-routing.module';

import { UserLogPage } from './user-log.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserLogPageRoutingModule
  ],
  declarations: [UserLogPage]
})
export class UserLogPageModule {}
