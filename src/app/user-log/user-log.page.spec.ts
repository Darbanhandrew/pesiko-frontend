import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserLogPage } from './user-log.page';

describe('UserLogPage', () => {
  let component: UserLogPage;
  let fixture: ComponentFixture<UserLogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserLogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
