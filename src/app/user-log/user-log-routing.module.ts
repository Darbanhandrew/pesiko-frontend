import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserLogPage } from './user-log.page';

const routes: Routes = [
  {
    path: '',
    component: UserLogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserLogPageRoutingModule {}
