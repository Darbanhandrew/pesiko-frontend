import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { CalendarOptions, DateSelectArg, EventApi, EventClickArg, FullCalendarComponent } from '@fullcalendar/angular'; // useful for typechecking
import faLocale from '@fullcalendar/core/locales/fa';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { DateClickArg } from '@fullcalendar/interaction';
import { AUTHTOKEN } from '../constants';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.page.html',
  styleUrls: ['./reserve.page.scss'],
})
export class ReservePage implements OnInit {

  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  calendarOptions: CalendarOptions = {
    plugins: [ timeGridPlugin, interactionPlugin],
    initialView: 'timeGrid',
    duration: { days: 4 },
    // titleFormat:{
    //  day:"numeric"
    // },
    dayHeaderFormat:{
      day:"numeric",
      weekday: 'short'
    },
    eventBackgroundColor :"#d6d7dc",
    hiddenDays:[5],
    selectable: true,
    weekends: true,
    editable: true,
    selectMirror: true,
    dateClick: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    // dateClick: this.handleDateClick.bind(this), // bind is important!
    events: [
      // { title: 'event 1', date: '2020-09-01' },
      // { title: 'event 2', date: '2020-09-22' }
    ],
    // dateClick: function(info) {
    //   alert('Clicked on: ' + info.dateStr);
    //   alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
    //   alert('Current view: ' + info.view.type);
    //   // change the day's background color just for fun
    //   info.dayEl.style.backgroundColor = 'red';
    // },
    slotDuration: '01:00',
    slotMinTime:'08:00',
    allDaySlot:false,
    locale: faLocale
  };
  currentEvents: EventApi[] = [];
  
  handleDateSelect(selectInfo: DateClickArg) {
    const title = prompt('Please enter a new title for your event');
    const calendarApi = selectInfo.view.calendar;
    console.log(calendarApi.getEvents())

    calendarApi.unselect(); // clear date selection
    
    if (title) {
      calendarApi.addEvent({
        // title,
        // start:selectInfo.start
        // start: selectInfo.startStr,
        // end: selectInfo.endStr,
        title:title, date: selectInfo.dateStr,
      });
      // console.log(selectInfo.allDay);
      // console.log(selectInfo.start);
      // console.log(selectInfo.end);
      // console.log(selectInfo.endStr);
    }
    
  }

  ngOnInit() {
    setTimeout(() => {
this.calendarComponent.getApi().updateSize();

});
  }
  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
    console.log("handleevent")
  }
  handleEventClick(clickInfo: EventClickArg) {
    if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      clickInfo.event.remove();
    }
    console.log("eventclick")
  }
}
