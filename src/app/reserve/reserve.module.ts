import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservePageRoutingModule } from './reserve-routing.module';

import { ReservePage } from './reserve.page';
import { FullCalendarModule } from '@fullcalendar/angular';
import { TestAnsComponent } from '../test-ans/test-ans.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservePageRoutingModule,
    FullCalendarModule
  ],
  declarations: [ReservePage]
})
export class ReservePageModule {}
