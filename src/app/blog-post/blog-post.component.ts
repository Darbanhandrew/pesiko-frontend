import { Component, Input, OnInit } from '@angular/core';
import { Path } from 'graphql/jsutils/Path';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.scss'],
})
export class BlogPostComponent implements OnInit {

  @Input() title:string;
  @Input() type:string;
  @Input() src:Path;
  @Input() description:string;
  constructor() { }

  ngOnInit() {}

}
