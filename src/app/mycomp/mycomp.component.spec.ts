import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MycompComponent } from './mycomp.component';

describe('MycompComponent', () => {
  let component: MycompComponent;
  let fixture: ComponentFixture<MycompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycompComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MycompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
