import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursePlayPage } from './course-play.page';

const routes: Routes = [
  {
    path: '',
    component: CoursePlayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursePlayPageRoutingModule {}
