import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursePlayPageRoutingModule } from './course-play-routing.module';

import { CoursePlayPage } from './course-play.page';
import { SharedModuleModule } from '../sharedmodule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursePlayPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [CoursePlayPage]
})
export class CoursePlayPageModule {}
