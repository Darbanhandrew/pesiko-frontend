import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoursePlayPage } from './course-play.page';

describe('CoursePlayPage', () => {
  let component: CoursePlayPage;
  let fixture: ComponentFixture<CoursePlayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursePlayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoursePlayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
