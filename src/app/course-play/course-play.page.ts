import { NullTemplateVisitor } from '@angular/compiler';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { throwServerError } from '@apollo/client/core';
import { elementMatches } from '@fullcalendar/core';
import { VideoPlayer } from '@ionic-native/video-player';
import { url } from 'inspector';


var path="content";
export interface vid {
  id:number;
  src:string;
}
@Component({
  selector: 'app-course-play',
  templateUrl: './course-play.page.html',
  styleUrls: ['./course-play.page.scss'],
})
export class CoursePlayPage implements OnInit {
  url='https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4';
  // src : any ;
  currentPlaying=null;
  course_vid:vid[]=[
    {
      id:1,
      src:'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4',
    },
    {
      id:2,
      src:'https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4',
    },
    {
      id:3,
      src:'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4',
    }
  ];

  
  @ViewChild('player')player:any;
  constructor() { }
  // video_src:string;
  // video_src:string;
  // video_src :'https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_Z5vR0tk6pUuROiHXtkoGl84zj0M86XPhrZiwT4lK.mp4';
  // https://player.arvancloud.com/index.html?config=https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_config.json
  ngOnInit() {
  }


  // didScroll(){
  //   if(this.currentPlaying && this.isElementInViewport(this.currentPlaying)){
  //     return;
  //   }
  //   else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)){
  //     this.currentPlaying=null;
  //   }
  // }


  openFullscreen(elem){
    if(elem.requestFullscreen) {
      elem.requestFullscreen();
    }
    else if (elem.webkitEnterFullscreen){
      elem.webkitEnterFullscreen();
      elem.enterFullscreen(); 
    }
  }

  
  play(path){
    // this.videoPlayers.forEach(player =>{
    this.player.nativeElement.src = path;
    this.currentPlaying = this.player.nativeElement;
    this.currentPlaying.play();
    // console.log(this.currentPlaying)
    // });
  }

  // toggle(){
  //   let video = document.getElementById('player');
  //   if(video.play){
  //     this.player.pause();
  //     console.log("1");
  //   }
  // }

  // isElementInViewport(el){
  //   const rect =el.getBoundingClientRect();
  //   return (
  //     rect.top>= 0 &&
  //     rect.left>= 0 &&
  //     rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
  //     rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  //   );
  // }
}
