import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditPageRoutingModule } from './credit-routing.module';

import { CreditPage } from './credit.page';
import { BluebuttonComponent } from '../bluebutton/bluebutton.component';
import { ngModuleJitUrl } from '@angular/compiler';
import { SharedModuleModule } from '../sharedmodule.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [CreditPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CreditPageModule {}
