import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-comp',
  templateUrl: './course-comp.component.html',
  styleUrls: ['./course-comp.component.scss'],
})
export class CourseCompComponent implements OnInit {

  constructor() { }
  @Input() title;
  ngOnInit() {}

}
