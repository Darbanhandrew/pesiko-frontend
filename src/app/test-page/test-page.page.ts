import { Component, OnInit,ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.page.html',
  styleUrls: ['./test-page.page.scss'],
  encapsulation: ViewEncapsulation.None // <-- add this line
})
export class TestPagePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
