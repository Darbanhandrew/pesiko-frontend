import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestPagePageRoutingModule } from './test-page-routing.module';

import { TestPagePage } from './test-page.page';
import { TestComponent } from '../test/test.component';

import { FormioModule } from '@formio/angular'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestPagePageRoutingModule,
    FormioModule
  ],
  declarations: [TestPagePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class TestPagePageModule {}
