import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TherapyGenPage } from './therapy-gen.page';

describe('TherapyGenPage', () => {
  let component: TherapyGenPage;
  let fixture: ComponentFixture<TherapyGenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapyGenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TherapyGenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
