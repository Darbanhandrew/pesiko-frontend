import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TherapyGenPageRoutingModule } from './therapy-gen-routing.module';
import { StarRatingModule } from 'angular-star-rating';
import { TherapyGenPage } from './therapy-gen.page';
import { TherapistCardComponent } from '../therapist-card/therapist-card.component';
import { SharedModuleModule } from '../sharedmodule.module';
// import { TherapistCardMo } from '../therapist-card/therapist-card.';

CUSTOM_ELEMENTS_SCHEMA
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TherapyGenPageRoutingModule,
    StarRatingModule.forRoot(),
    SharedModuleModule
  ],
  declarations: [TherapyGenPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class TherapyGenPageModule {}
