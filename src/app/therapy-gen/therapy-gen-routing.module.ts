import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TherapyGenPage } from './therapy-gen.page';

const routes: Routes = [
  {
    path: '',
    component: TherapyGenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TherapyGenPageRoutingModule {}
