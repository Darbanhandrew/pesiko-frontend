import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-therapy-gen',
  templateUrl: './therapy-gen.page.html',
  styleUrls: ['./therapy-gen.page.scss'], // add NgbRatingConfig to the component providers
})
// export class NgbdRatingConfig {
//   constructor(config: NgbRatingConfig) {
//     // customize default values of ratings used by this component tree
//     config.max = 5;
//     config.readonly = true;
//   }
// }
export class TherapyGenPage implements OnInit {

  
  name:String;
  path:String;
  constructor(private router:Router,) { }
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  disorders:any[] = [
    {
      name:"اختلال کم خوابی",
      path:"dis3"
    },
    {
      name:"اختلال کم خوابی",
      path:"dis3"
    },
    {
      name:"اختلال کم خوابی",
      path:"dis3"
    }
  ];
  therapists:any[] = [
    {
      name:"دکتر محسن سلامی",
      path:"dis1"
    },
    {
      name:"دکتر علی بلایی",
      path:"dis2"
    },
    {
      name:"دکتر علی بلایی",
      path:"dis2"
    }
  ];
  ngOnInit() {
  }

  therapist_click(){
    this.router.navigate(['/reserve'])
  }
}
