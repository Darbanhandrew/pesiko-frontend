import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursePageRoutingModule } from './course-routing.module';

import { CoursePage } from './course.page';
import { BluebuttonComponent } from '../bluebutton/bluebutton.component';
import { TestAnsComponent } from '../test-ans/test-ans.component';
import { CourseCompComponent } from '../course-comp/course-comp.component';
import { SharedModuleModule } from '../sharedmodule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursePageRoutingModule,
    SharedModuleModule
  ],
  declarations: [CoursePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class CoursePageModule {}
