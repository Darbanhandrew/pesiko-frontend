import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SleepPodcastPage } from './sleep-podcast.page';

describe('SleepPodcastPage', () => {
  let component: SleepPodcastPage;
  let fixture: ComponentFixture<SleepPodcastPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SleepPodcastPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SleepPodcastPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
