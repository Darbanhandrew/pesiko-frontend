import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SleepPodcastPage } from './sleep-podcast.page';

const routes: Routes = [
  {
    path: '',
    component: SleepPodcastPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SleepPodcastPageRoutingModule {}
