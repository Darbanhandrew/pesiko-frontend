import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SleepPodcastPageRoutingModule } from './sleep-podcast-routing.module';

import { SleepPodcastPage } from './sleep-podcast.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SleepPodcastPageRoutingModule
  ],
  declarations: [SleepPodcastPage]
})
export class SleepPodcastPageModule {}
