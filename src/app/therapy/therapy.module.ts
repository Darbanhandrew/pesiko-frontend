import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TherapyPageRoutingModule } from './therapy-routing.module';

import { TherapyPage } from './therapy.page';
import { FourSquareComponent } from '../four-square/four-square.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TherapyPageRoutingModule,
    RouterModule.forChild([{ path: '', component: TherapyPage }]),
    TherapyPageRoutingModule
  ],
  declarations: [TherapyPage,FourSquareComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class TherapyPageModule {}
