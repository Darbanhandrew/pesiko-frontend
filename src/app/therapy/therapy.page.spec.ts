import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TherapyPage } from './therapy.page';

describe('TherapyPage', () => {
  let component: TherapyPage;
  let fixture: ComponentFixture<TherapyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TherapyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TherapyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
