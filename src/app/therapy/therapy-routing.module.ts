import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TherapyPage } from './therapy.page';

const routes: Routes = [
  {
    path: '',
    component: TherapyPage,
    // children: [
    //   {
        
    //     path: 'conditions-list',
    //     loadChildren: () => import('../conditions-list/conditions-list.module').then(m => m.ConditionsListPageModule)
    //   },
    //   {
    //     path: 'tab2',
    //     loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
    //   },
    //   {
    //     path: 'tab3',
    //     loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
    //   },
    //   {
    //     path: '',
    //     // redirectTo: '/',
    //     pathMatch: 'full'
    //   }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TherapyPageRoutingModule {}
