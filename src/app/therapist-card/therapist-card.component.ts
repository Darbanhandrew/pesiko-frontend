import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-therapist-card',
  templateUrl: './therapist-card.component.html',
  styleUrls: ['./therapist-card.component.scss'],
})
export class TherapistCardComponent implements OnInit {
  
  @Input() name : string ;
  constructor(private router:Router) { }

  ngOnInit() {}

  therapist_click(){
    this.router.navigate(['/reserve'])
  }
}
