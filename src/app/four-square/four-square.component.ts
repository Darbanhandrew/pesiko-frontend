import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-four-square',
  templateUrl: './four-square.component.html',
  styleUrls: ['./four-square.component.scss'],
})
export class FourSquareComponent implements OnInit {

  constructor() { }
  @Input() text1: string;
  @Input() text2: string;
  @Input() text3: string;
  @Input() text4: string;
  @Input() link1: string;
  @Input() link2: string;
  @Input() link3: string;
  @Input() link4: string;
  
  ngOnInit() {}

}
