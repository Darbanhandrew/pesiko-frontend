import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PodcastPagePage } from './podcast-page.page';

const routes: Routes = [
  {
    path: '',
    component: PodcastPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PodcastPagePageRoutingModule {}
