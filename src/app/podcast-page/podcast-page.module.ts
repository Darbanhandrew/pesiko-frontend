import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MusicplayerComponent } from '../musicplayer/musicplayer.component';
import { IonicModule } from '@ionic/angular';

import { PodcastPagePageRoutingModule } from './podcast-page-routing.module';

import { PodcastPagePage } from './podcast-page.page';
import { PodcastComponent } from '../podcast/podcast.component';
import { PlayerProvider } from 'src/providers/player/player';
import { SharedModuleModule } from '../sharedmodule.module';
import { MusicModalPage } from '../music-modal/music-modal.page';
import { AudioService } from '../audio.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PodcastPagePageRoutingModule,
    IonicModule,
    SharedModuleModule
  ],
  providers: [
    PlayerProvider,
    AudioService
],
  declarations: [PodcastPagePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  entryComponents:[PodcastComponent,MusicModalPage]
})
export class PodcastPagePageModule {}
