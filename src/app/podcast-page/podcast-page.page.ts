import { NgModule,Component, OnInit, ViewChild } from '@angular/core';
import { ModalController , IonRange, IonicModule} from '@ionic/angular';
import { MusicplayerComponent } from '../musicplayer/musicplayer.component';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { File } from '@ionic-native/file/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import {Howl} from 'howler';
import { pathToArray } from 'graphql/jsutils/Path';
import { ngModuleJitUrl } from '@angular/compiler';
import { MusicModalPage } from '../music-modal/music-modal.page';
import { AudioService } from '../audio.service';


declare var window;
export interface Track {
  name:String;
  path:String;
  part:any;
}
@Component({
  selector: 'app-podcast-page',
  templateUrl: './podcast-page.page.html',
  styleUrls: ['./podcast-page.page.scss'],
})

export class PodcastPagePage implements OnInit {
  @ViewChild('range',{static:false}) range: IonRange ;
  playlist=this.serv.playlist;
  progress=this.serv.progress;
  constructor(private modalCtrl:ModalController , public serv:AudioService) { 
    
  }


  // activeTrack=null;
  // player =null;
  // isPlaying = null
  
  // progress=this.serv.progress;
  ngOnInit() {
    // this.activeTrack=this.serv.activetrack;
    // this.player =this.serv.player;
    // this.isPlaying = this.serv.state;
  }
  
  start(track :Track){
    this.serv.start(track);
  }


  togglePlayer(pause){
    this.serv.togglePlayer(pause);
  }

  prev(){
    this.serv.prev();
  }

  seek(){
    let r =this.range.value;
    this.serv.seek(r);
  }

  updateProgress(){
    this.serv.updateprogress();
  }

  async openModal(audio){

    // let data=this.serv.track;
    // this.serv.track=this.activeTrack;
    const modal = await this.modalCtrl.create({
      component:MusicModalPage,
      componentProps: { 
        // costum_id:this.activeTrack,
        // text:'amir',
        // amir :{
        //   name:'amir',
        //   path:'https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_Z5vR0tk6pUuROiHXtkoGl84zj0M86XPhrZiwT4lK.mp4'
        // } 
      }
    });
    // console.log(this.serv.name)
    
    await modal.present();
    
  }
}
