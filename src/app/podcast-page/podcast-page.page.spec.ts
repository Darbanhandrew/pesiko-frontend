import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PodcastPagePage } from './podcast-page.page';

describe('PodcastPagePage', () => {
  let component: PodcastPagePage;
  let fixture: ComponentFixture<PodcastPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PodcastPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PodcastPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
