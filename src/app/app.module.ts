import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy , } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import {HttpClientModule, HttpHeaders} from '@angular/common/http';
import {APOLLO_OPTIONS, Apollo} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache, ApolloLink} from '@apollo/client/core';
import { AuthService } from './auth.service';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { FormioModule } from 'angular-formio';
import { AUTHTOKEN } from './constants';
import { CardComponentModule } from './card/card.component.module';
import { CardComponent } from './card/card.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { TabsService } from './core/tabs.service';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import { MoodloginComponent } from './moodlogin/moodlogin.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import { Tab3Page } from './tab3/tab3.page';
import { SharedModuleModule } from './sharedmodule.module';
import { MusicModalPageModule } from './music-modal/music-modal.module';
import { ChatService } from './chat.service';

CUSTOM_ELEMENTS_SCHEMA
const token = localStorage.getItem(AUTHTOKEN);
FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin

]);
@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [AppComponent],
  entryComponents: [],
  
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FormioModule,
    CommonModule,
    BrowserModule,
    FullCalendarModule,
    NgbModule,
    IonicModule,
    SharedModuleModule,
    MusicModalPageModule
    // TherapistCardComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    TabsService,
    ChatService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: APOLLO_OPTIONS,
      useFactory: (httpLink: HttpLink) => {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: 'http://localhost:8000/graphql',
            headers:new HttpHeaders(
              {
                Authorization:`JWT ${token}`
              }
            )
          }),
        };
      },
      deps: [HttpLink],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
