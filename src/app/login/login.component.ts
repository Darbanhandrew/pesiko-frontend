import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Apollo } from 'apollo-angular';
import { USERNAME, AUTHTOKEN, REFRESHTOKEN } from '../constants';
import { LOGIN_MUTATION,LoginMutationResponse, REGISTER_MUTATION, RegisterMutationResponse} from '../graphql';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  login: boolean=true;
  username: string='';
  password: string='';
  email: string='';
  constructor(private router: Router,
    private authService: AuthService,
    private apollo: Apollo) { }

  ngOnInit() {}
  
  confirm(){
    if (this.login) {
      this.apollo.mutate<LoginMutationResponse>({
        mutation: LOGIN_MUTATION,
        variables: {
          username: this.username,
          password: this.password
        }
      }).subscribe((result) => {
        console.log(result);
        const username = this.username;
        const token = result.data.tokenAuth.token;
        const refreshToken= result.data.tokenAuth.refreshToken;
        this.saveUserData(username, token, refreshToken);

        this.router.navigate(['/']);

      }, (error) => {
        alert(error)
      });
    } else {
      this.apollo.mutate<RegisterMutationResponse>({
        mutation: REGISTER_MUTATION,
        variables: {
          email: this.email,
          username: this.username,
          password1: this.password,
          password2: this.password,
        }
      }).subscribe((result) => {
        if(result.data.register.success)
        {
        const token = result.data.register.token;
        const username = this.username;
        const refreshToken=result.data.register.refreshToken;
        this.saveUserData(username, token, refreshToken);
        }
        this.router.navigate(['/']);

      }, (error) => {
        alert(error)
      });
    }
  }
  saveUserData(username, token, refreshToken)
  {
    localStorage.setItem(USERNAME, username);
    localStorage.setItem(AUTHTOKEN, token);
    localStorage.setItem(REFRESHTOKEN,refreshToken);
    this.authService.setusername(username);
  }
}
