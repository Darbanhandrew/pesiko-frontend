import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MeditationPageRoutingModule } from './meditation-routing.module';

import { MeditationPage } from './meditation.page';
import { PodcastButtonComponent } from '../podcast-button/podcast-button.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MeditationPageRoutingModule
  ],
  declarations: [MeditationPage,PodcastButtonComponent]
})
export class MeditationPageModule {}
