import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgetPassPageRoutingModule } from './forget-pass-routing.module';

import { ForgetPassPage } from './forget-pass.page';
import { CardComponent } from '../card/card.component';
import { CardComponentModule } from '../card/card.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgetPassPageRoutingModule,
    CardComponentModule
  ],
  declarations: [ForgetPassPage]
})
export class ForgetPassPageModule {}
