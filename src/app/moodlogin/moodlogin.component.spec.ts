import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MoodloginComponent } from './moodlogin.component';

describe('MoodloginComponent', () => {
  let component: MoodloginComponent;
  let fixture: ComponentFixture<MoodloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodloginComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MoodloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
