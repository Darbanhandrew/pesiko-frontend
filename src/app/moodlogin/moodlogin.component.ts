import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-moodlogin',
  templateUrl: './moodlogin.component.html',
  styleUrls: ['./moodlogin.component.scss'],
})
export class MoodloginComponent implements OnInit {

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}

  confirm(){
    this.modalCtrl.dismiss();
  }
}
