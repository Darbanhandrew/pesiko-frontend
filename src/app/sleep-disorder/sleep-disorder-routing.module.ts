import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SleepDisorderPage } from './sleep-disorder.page';

const routes: Routes = [
  {
    path: '',
    component: SleepDisorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SleepDisorderPageRoutingModule {}
