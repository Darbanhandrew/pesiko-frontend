import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SleepDisorderPageRoutingModule } from './sleep-disorder-routing.module';
import { StarRatingModule } from 'angular-star-rating';
import { SleepDisorderPage } from './sleep-disorder.page';
import { TherapistCardComponent } from '../therapist-card/therapist-card.component';
import { TherapyGenPageModule } from '../therapy-gen/therapy-gen.module';
import { SharedModuleModule } from '../sharedmodule.module';

CUSTOM_ELEMENTS_SCHEMA
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SleepDisorderPageRoutingModule,
    StarRatingModule,
    TherapyGenPageModule,
    SharedModuleModule
  ],
  declarations: [SleepDisorderPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SleepDisorderPageModule {}
