import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SleepDisorderPage } from './sleep-disorder.page';

describe('SleepDisorderPage', () => {
  let component: SleepDisorderPage;
  let fixture: ComponentFixture<SleepDisorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SleepDisorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SleepDisorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
