import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeReservePageRoutingModule } from './time-reserve-routing.module';

import { TimeReservePage } from './time-reserve.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeReservePageRoutingModule
  ],
  declarations: [TimeReservePage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class TimeReservePageModule {
 
}
