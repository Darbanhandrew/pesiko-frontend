import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeReservePage } from './time-reserve.page';

const routes: Routes = [
  {
    path: '',
    component: TimeReservePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeReservePageRoutingModule {}
