import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TimeReservePage } from './time-reserve.page';

describe('TimeReservePage', () => {
  let component: TimeReservePage;
  let fixture: ComponentFixture<TimeReservePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeReservePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TimeReservePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
