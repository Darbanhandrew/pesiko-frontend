import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-card',
  templateUrl: './test-card.component.html',
  styleUrls: ['./test-card.component.scss'],
})
export class TestCardComponent implements OnInit {

  constructor() { }
  @Input() link:string;
  @Input() text:string;
  ngOnInit() {}

}
