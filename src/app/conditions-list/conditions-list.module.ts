import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConditionsListPageRoutingModule } from './conditions-list-routing.module';

import { ConditionsListPage } from './conditions-list.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConditionsListPageRoutingModule,
    ExploreContainerComponentModule,
  ],
  declarations: [ConditionsListPage]
})
export class ConditionsListPageModule {}
