import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConditionsListPage } from './conditions-list.page';

describe('ConditionsListPage', () => {
  let component: ConditionsListPage;
  let fixture: ComponentFixture<ConditionsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConditionsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
