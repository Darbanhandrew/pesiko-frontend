import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ConditionListResponse, CONDITIONS_LIST, ConditionResponse } from '../graphql';

@Component({
  selector: 'app-conditions-list',
  templateUrl: './conditions-list.page.html',
  styleUrls: ['./conditions-list.page.scss'],
})
export class ConditionsListPage implements OnInit {

  conditions: ConditionResponse[]
  constructor(private apollo: Apollo) { }

  ngOnInit() {
    const queryConditions =this.apollo.watchQuery<ConditionListResponse>(
      {
        query: CONDITIONS_LIST,
      }
      ).valueChanges.subscribe((result) => {
        this.conditions=result.data.conditions;
      });
  }

}
