import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavsPageRoutingModule } from './favs-routing.module';

import { FavsPage } from './favs.page';
import { BluebuttonComponent } from '../bluebutton/bluebutton.component';

CUSTOM_ELEMENTS_SCHEMA
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavsPageRoutingModule
  ],
  declarations: [FavsPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class FavsPageModule {}
