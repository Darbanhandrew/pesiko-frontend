import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerificationPageRoutingModule } from './verification-routing.module';

import { VerificationPage } from './verification.page';
import { CardComponentModule } from '../card/card.component.module';
import { MoodloginComponent } from '../moodlogin/moodlogin.component';
import { TestCardComponent } from '../test-card/test-card.component';
import { PodcastComponent } from '../podcast/podcast.component';
import { FourSquareComponent } from '../four-square/four-square.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerificationPageRoutingModule,
    CardComponentModule,
  ],
  declarations: [VerificationPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class VerificationPageModule {}
