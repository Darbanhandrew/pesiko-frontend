
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, JsonpClientBackend } from '@angular/common/http';
// import {Observable} from "rxjs/Observable";
import { Router } from '@angular/router';
import { createHostListener } from '@angular/compiler/src/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {Config, LoadingController} from '@ionic/angular';
import { parseAndCheckHttpResponse } from '@apollo/client/core';
import { Json } from 'twilio/lib/interfaces';
import * as objectPath from 'object-path'


@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})

export class PaymentPage implements OnInit {
  public token:any;
   amount:any;
   public path:any;
  // courses$: Observable;
  constructor(private http : HttpClient , public router:Router,public loadingController: LoadingController) { 
    
  }
  
  ngOnInit() {
  }

  
   
  async  func(){
    const loading = await this.loadingController.create({
      message: 'Please wait...'
      });
      loading.present();
    // this.http.post("https://pay.ir/pg/send?api=test&amount=100000&redirect=1",[]).subscribe(data => {
    //   this.postId = data.id;

    // )
    // // console.log(a);
    let params ={
      api :"test",
      amount:this.amount,
      redirect:"localhost:8100/payment-res"
    }
    console.log(this.amount)
    this.http.post("https://pay.ir/pg/send", params).subscribe(
     
      res => {
        let res1=JSON.stringify(res)
        let resSTR = JSON.parse(res1)
          this.token = resSTR.token;

          console.log(this.token);
          loading.dismiss();
          this.path="https://pay.ir/pg/" + this.token;
          window.location.href = this.path;
        }
      
    );
    // JSON.parse(this.http.post("https://pay.ir/pg/send", params));

    // if(this.token){
    // this.path="https://pay.ir/pg/" + this.token;
    // window.location.href = this.path;
    // }
    
    // sequence.subscribe({
    //   next(num) { 
    //     // console.log(num.token); 
    //     // return(num.token);
    //   // this.path="https://pay.ir/pg/"+String(this.token);
    //   // this.router.navigate([this.path]);
        
    //   },
    // });

    //   sequence.subscribe(
    //   res => {
    //   this.token=res.token;
    // }
    // );
    

    // console.log(this.token);
    
    
  }

  
  // button2(){

  //   // this.router.navigate([this.path]);
  //   this.path="https://pay.ir/pg/" + this.token;
  //   window.location.href = this.path;

  //   // this.iab.open("https://www.w3schools.com/php/");
  //   // console.log(this.token);

  //   // let a = this.http.get("https://pay.ir/pg/",this.token)
  //   // this.courses$ = this.http
  //   //         .get("/courses.json")
  //   //         .map(data => _.values(data))
  //   //         .do(console.log);
  //   // a.subscribe(
  //   //   res => console.log(1)
  //   // );
  //   // console.log(a);
  //   // a.subscribe(
  //   //   {next(res){
  //   //     console.log('a')
  //   //   },
  //   //   });
  // }
  

}
