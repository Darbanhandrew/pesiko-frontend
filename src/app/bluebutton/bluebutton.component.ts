import { Component, OnInit , Input,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

CUSTOM_ELEMENTS_SCHEMA

@Component({
  selector: 'app-bluebutton',
  templateUrl: './bluebutton.component.html',
  styleUrls: ['./bluebutton.component.scss'],
})
export class BluebuttonComponent implements OnInit {

  constructor() { }
  @Input() text1: string;
  @Input() link1: string;
  ngOnInit() {}

  onClick(){
    console.log("its ok")
  }
}
