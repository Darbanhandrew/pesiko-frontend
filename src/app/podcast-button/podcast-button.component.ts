import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-podcast-button',
  templateUrl: './podcast-button.component.html',
  styleUrls: ['./podcast-button.component.scss'],
})
export class PodcastButtonComponent implements OnInit {

  constructor(private router:Router) { }
  @Input() title;
  @Input() duration;
  @Input() link;
  ngOnInit() {}

  navigate(){
    this.router.navigate([this.link])
  }
}
