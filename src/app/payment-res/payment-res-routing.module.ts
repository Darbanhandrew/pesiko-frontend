import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentResPage } from './payment-res.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentResPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentResPageRoutingModule {}
