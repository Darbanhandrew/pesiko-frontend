import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymentResPage } from './payment-res.page';

describe('PaymentResPage', () => {
  let component: PaymentResPage;
  let fixture: ComponentFixture<PaymentResPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentResPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymentResPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
