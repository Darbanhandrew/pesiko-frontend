import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentResPageRoutingModule } from './payment-res-routing.module';

import { PaymentResPage } from './payment-res.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentResPageRoutingModule
  ],
  declarations: [PaymentResPage]
})
export class PaymentResPageModule {}
