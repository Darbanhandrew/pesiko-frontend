
import gql from 'graphql-tag'

export const LOGIN_MUTATION=gql`
mutation LoginMutation($username:String!,$password:String!){
    tokenAuth(
      username: $username,
      password: $password,
    ) {
      token,
      refreshToken,
    }
  }
`;
export interface LoginMutationResponse
{
    tokenAuth:{
      token:string;
      refreshToken:string;
    }
}

export const REGISTER_MUTATION=gql`
mutation RegisterMutation($email: String!, $username:String!,$password1:String!,$password2:String!){
    register(
        email: $email,
        username: $username,
        password1: $password1,
        password2: $password2,
    ) {
      success,
      errors,
      token,
      refreshToken
    }
}
   
`;
export interface RegisterMutationResponse
{
  register:
  {
    success:boolean;
    erros:string;
    refreshToken:string;
    token:string;
}
  }
export const CONDITIONS_LIST=gql`

  query ConditionsList
  {
    conditions{
      id,
      title,
      intro,
    },
  }
`
export interface ConditionListResponse
{
  conditions: ConditionResponse[]

}
export interface ConditionResponse 
{
  id : number;
  title: string;
  intro : string;
}