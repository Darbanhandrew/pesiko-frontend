import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelfConPageRoutingModule } from './self-con-routing.module';

import { SelfConPage } from './self-con.page';
import { TestCardComponent } from '../test-card/test-card.component';
import { BluebuttonComponent } from '../bluebutton/bluebutton.component';
import { SharedModuleModule } from '../sharedmodule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelfConPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [SelfConPage],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SelfConPageModule {}
