import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelfConPage } from './self-con.page';

const routes: Routes = [
  {
    path: '',
    component: SelfConPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelfConPageRoutingModule {}
