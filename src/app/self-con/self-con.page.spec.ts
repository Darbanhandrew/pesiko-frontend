import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelfConPage } from './self-con.page';

describe('SelfConPage', () => {
  let component: SelfConPage;
  let fixture: ComponentFixture<SelfConPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfConPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelfConPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
