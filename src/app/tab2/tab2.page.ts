import { Component } from '@angular/core';
import { MusicplayerComponent } from '../musicplayer/musicplayer.component';
import { ModalController , IonRange} from '@ionic/angular';
import { TaskAddComponent } from '../task-add/task-add.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  title:string;
  description:string;
  constructor(private modalCtrl:ModalController) {}
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  todolist:any[] = [
    {
      title:"therapy session",
      start_time:"19:00",
      end_time : "18:00"

    },
    {
      title:"therapy session",
      start_time:"19:00",
      end_time : "18:00"

    },
    {
      title:"therapy session",
      start_time:"19:00",
      end_time : "18:00"

    }
  ]

  async add(){
    const modal = await this.modalCtrl.create({
      component:TaskAddComponent
    });

    await modal.present();
    
  }
}
