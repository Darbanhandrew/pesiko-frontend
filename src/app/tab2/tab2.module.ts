import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { Tab2PageRoutingModule } from './tab2-routing.module';
import { CardComponentModule } from '../card/card.component.module';
import { CardComponent } from '../card/card.component';
import { LogincardComponent } from '../logincard/logincard.component';
import { TestComponent } from '../test/test.component';
import { TestCardComponent } from '../test-card/test-card.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab2PageRoutingModule,
    CardComponentModule
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
