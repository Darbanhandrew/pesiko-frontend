import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab3Page } from './tab3.page';

const routes: Routes = [
  {
    path: '',
    component: Tab3Page,
    // children: [
    //   {
    //     path: 'self-con',
    //     loadChildren: () => import('../self-con/self-con.module').then(m => m.SelfConPageModule)
    //   },
    // ]
  }
  // {
  //   path: 'courses',
  //   loadChildren: () => import('./courses/courses.module').then( m => m.CoursesPageModule)
  // },
  // {
  //   path: 'course',
  //   loadChildren: () => import('./course/course.module').then( m => m.CoursePageModule)
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
