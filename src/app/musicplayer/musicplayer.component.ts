import { Component, Input, NgModule, OnInit,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicModule,ModalController } from '@ionic/angular';


export interface Track {
  name:String;
  path:String;
}

// @public({
//   @NgModule.schemas:[
//     CUSTOM_ELEMENTS_SCHEMA
//   ]
// })
// @NgModule({
//   schemas:[CUSTOM_ELEMENTS_SCHEMA],
// })
@Component({
  selector: 'app-musicplayer',
  templateUrl: './musicplayer.component.html',
  styleUrls: ['./musicplayer.component.scss'],
})
export class MusicplayerComponent implements OnInit {
  @Input() now: Track;

  playlist :Track[] =[
    {
      name : 'test',
      path :'https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_Z5vR0tk6pUuROiHXtkoGl84zj0M86XPhrZiwT4lK.mp4'
    },
    {
      name : 'test2',
      path :'https://mizanscene.arvanvod.com/A8LXoP4qBw/eap2aKkg95/origin_Z5vR0tk6pUuROiHXtkoGl84zj0M86XPhrZiwT4lK.mp4'
    }
  ];

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}

  dissmissModal(){
    this.modalCtrl.dismiss();
      }

}
