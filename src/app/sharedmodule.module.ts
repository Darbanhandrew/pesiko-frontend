import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StarRatingModule } from 'angular-star-rating';

import { IonicModule } from '@ionic/angular';
import { TestCardComponent } from './test-card/test-card.component';
import { TestComponent } from './test/test.component';
import { VerificationPage } from './verification/verification.page';
import { VerificationPageModule } from './verification/verification.module';
import { PodcastComponent } from './podcast/podcast.component';
import { TaskAddComponent } from './task-add/task-add.component';
import { TestPagePageModule } from './test-page/test-page.module';
import { TherapistCardComponent } from './therapist-card/therapist-card.component';
import { BluebuttonComponent } from './bluebutton/bluebutton.component';
import { LogincardComponent } from './logincard/logincard.component';
import { TestAnsComponent } from './test-ans/test-ans.component';
import { MusicplayerComponent } from './musicplayer/musicplayer.component';
import { CourseCompComponent } from './course-comp/course-comp.component';
// import { LoginComponent } from './login/login.component';
import { MoodloginComponent } from './moodlogin/moodlogin.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { VideoCompComponent } from './video-comp/video-comp.component';
// import { TherapistCardMo } from '../therapist-card/therapist-card.';

CUSTOM_ELEMENTS_SCHEMA
@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    StarRatingModule.forRoot(),
  ],
  exports:[TherapistCardComponent,MoodloginComponent,TestCardComponent,TestComponent,PodcastComponent
    ,VideoCompComponent,TaskAddComponent,BluebuttonComponent,LogincardComponent,TestAnsComponent,MusicplayerComponent,CourseCompComponent,BlogPostComponent],
  declarations: [TherapistCardComponent,MoodloginComponent,TestCardComponent,TestComponent,PodcastComponent
    ,VideoCompComponent,TaskAddComponent,BluebuttonComponent,LogincardComponent,TestAnsComponent,MusicplayerComponent,CourseCompComponent,BlogPostComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModuleModule {}
