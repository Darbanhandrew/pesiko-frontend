import { Component, NgModule, OnInit } from '@angular/core';
import { IonicModule,ModalController , IonRange} from '@ionic/angular';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss'],
})
export class TaskAddComponent implements OnInit {

  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}

  confirm(){
    this.modalCtrl.dismiss();
  }
}
