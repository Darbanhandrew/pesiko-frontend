import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {USERNAME, AUTHTOKEN, REFRESHTOKEN} from './constants';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // 2
  private username: string = null;

  // 3
  private _isAuthenticated = new BehaviorSubject(false);

  constructor() {
  }

  // 4

  // 5
  saveUserData(username: string, token: string, refreshtoken:string) {

    localStorage.setItem(USERNAME, username);
    localStorage.setItem(AUTHTOKEN, token);
    localStorage.setItem(REFRESHTOKEN, refreshtoken);
    this.setusername(username);
  }

  // 6
  setusername(username: string) {
    this.username = username;

    this._isAuthenticated.next(true);
  }
  // 7
  logout() {
    localStorage.removeItem(USERNAME);
    localStorage.removeItem(AUTHTOKEN);
    localStorage.removeItem(REFRESHTOKEN);
    this.username = null;

    this._isAuthenticated.next(false);
  }

  // 8
  autoLogin() {
    const username = localStorage.getItem(USERNAME);
    if (username) {
      this.setusername(username);

    }
  }
  
}
