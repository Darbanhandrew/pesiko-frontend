import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IonRange, ModalController, NavParams } from '@ionic/angular';
import { AudioService } from '../audio.service';
import { PodcastPagePage, Track } from '../podcast-page/podcast-page.page';

// import{pocast-page} from ',,/podcast-page/podcast-page.page'


declare var window;
@Component({
  selector: 'app-music-modal',
  templateUrl: './music-modal.page.html',
  styleUrls: ['./music-modal.page.scss'],
})
export class MusicModalPage implements OnInit {

  html5 = true;
  @ViewChild('range',{static:false}) range: IonRange ;
  constructor(private navParams:NavParams , private modalController:ModalController, public serv:AudioService) {
   }

  ngOnInit() {
    // this.passedId = this.navParams.get('custom_id');
    // this.a = this.navParams.get('text');
    // this.b=this.navParams.get('costum_id');
    console.log(this.serv.state)
  }

  closeModal(){
    this.modalController.dismiss();
  }

  toggle(e){
    this.serv.togglePlayer(e);
  }

  seek(){
    let r =this.range.value;
    this.serv.seek(r);
  }
}
