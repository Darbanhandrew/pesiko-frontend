import { EventEmitter,Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { AccessToken } from 'twilio';
// import { AccessToken } from 'twilio-chat';
import * as Twilio from 'twilio-chat';
import Client from "twilio-chat";
import {Channel} from "twilio-chat/lib/channel";
import twilio from 'twilio';


// declare var cryptojs
// const AccessToken = require('twilio').jwt.AccessToken;

@Injectable({
  providedIn: 'root'
})
export class ChatService {


  public chatClient: Client;
  public currentChannel: Channel;
  public chatConnectedEmitter: EventEmitter<any> = new EventEmitter<any>()
  public chatDisconnectedEmitter: EventEmitter<any> = new EventEmitter<any>()

  constructor(
    private router: Router,
  ) { }



  // connect(token) {
  //   // let client = require('twilio')("1", "2");
  // // client.region = 'au1';
  // // client.edge = 'sydney';
  //   //  token = new AccessToken(
  //   //   "ACee528dddf52357b4135b21afd2d45f23", "SK3c8903eb507a6b5cd922be4ee0bc9fc0","CDAIar8fM97jcYKN6fugpXwPUuJyfBLc"
  //   // );
  //   // let mytoken = new AccessToken("ACee528dddf52357b4135b21afd2d45f23", "SK3c8903eb507a6b5cd922be4ee0bc9fc0","CDAIar8fM97jcYKN6fugpXwPUuJyfBLc");
  //   // console.log(token)
  //   Twilio.Client.create("eyJ6aXAiOiJERUYiLCJraWQiOiJTQVNfUzNfX19LTVNfdjEiLCJjdHkiOiJ0d2lsaW8tZnBhO3Y9MSIsImVuYyI6IkEyNTZHQ00iLCJhbGciOiJkaXIifQ..qjltWfIgQaTwp2De.81Z_6W4kR-hdlAUvJQCbwS8CQ7QAoFRkOvNMoySEj8zEB4BAY3MXhPARfaK4Lnr4YceA2cXEmrzPKQ7bPm0XZMGYm1fqLYzAR8YAqUetI9WEdQLFytg1h4XnJnXhgd99eNXsLkpKHhsCnFkchV9eGpRrdrfB0STR5Xq0fdakomb98iuIFt1XtP0_iqxvxQZKe1O4035XhK_ELVwQBz_qdI77XRZBFM0REAzlnEOe61nOcQxkaIM9Qel9L7RPhcndcCPFAyYjxo6Ri5c4vOnszLDiHmeK9Ep9fRE5-Oz0px0ZEg_FgTUEPFPo2OHQj076H1plJnFr-qPINDJkUL_i7loqG1IlapOi1JSlflPH-Ebj4hhpBdMIcs-OX7jhqzmVqkIKWkpPyPEmfvY2-eA5Zpoo08YpqAJ3G1l_xEcHl28Ijkefj1mdb6E8POx41skAwXCpdfIbzWzV_VjFpmwhacS3JZNt9C4hVG4Yp-RGPEl1C7aJHRIUavAmoRHaXbfG20zzv5Zu0P5PcopDszzoqVfZpzc.GCt35DWTurtP-QaIL5aBSQ").then( (client: Client) => {
  //     this.chatClient = client;
  //     this.chatConnectedEmitter.emit(true);
  //   }).catch( (err: any) => {
  //     this.chatDisconnectedEmitter.emit(true);
  //     // if( err.message.indexOf('token is expired') ) {
  //       // localStorage.removeItem('twackToken');
  //       // this.router.navigate(['/']);
  //     }
  //   });
  // }

  connect(token) {
    Twilio.Client.create("eyJ6aXAiOiJERUYiLCJraWQiOiJTQVNfUzNfX19LTVNfdjEiLCJjdHkiOiJ0d2lsaW8tZnBhO3Y9MSIsImVuYyI6IkEyNTZHQ00iLCJhbGciOiJkaXIifQ..qjltWfIgQaTwp2De.81Z_6W4kR-hdlAUvJQCbwS8CQ7QAoFRkOvNMoySEj8zEB4BAY3MXhPARfaK4Lnr4YceA2cXEmrzPKQ7bPm0XZMGYm1fqLYzAR8YAqUetI9WEdQLFytg1h4XnJnXhgd99eNXsLkpKHhsCnFkchV9eGpRrdrfB0STR5Xq0fdakomb98iuIFt1XtP0_iqxvxQZKe1O4035XhK_ELVwQBz_qdI77XRZBFM0REAzlnEOe61nOcQxkaIM9Qel9L7RPhcndcCPFAyYjxo6Ri5c4vOnszLDiHmeK9Ep9fRE5-Oz0px0ZEg_FgTUEPFPo2OHQj076H1plJnFr-qPINDJkUL_i7loqG1IlapOi1JSlflPH-Ebj4hhpBdMIcs-OX7jhqzmVqkIKWkpPyPEmfvY2-eA5Zpoo08YpqAJ3G1l_xEcHl28Ijkefj1mdb6E8POx41skAwXCpdfIbzWzV_VjFpmwhacS3JZNt9C4hVG4Yp-RGPEl1C7aJHRIUavAmoRHaXbfG20zzv5Zu0P5PcopDszzoqVfZpzc.GCt35DWTurtP-QaIL5aBSQ").then( (client: Client) => {
      this.chatClient = client;
      this.chatConnectedEmitter.emit(true);
    }).catch( (err: any) => {
      this.chatDisconnectedEmitter.emit(true);
      if( err.message.indexOf('token is expired') ) {
        // localStorage.removeItem('twackToken');
        // this.router.navigate(['/']);
      }
    });
  }
}
