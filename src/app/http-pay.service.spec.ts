import { TestBed } from '@angular/core/testing';

import { HttpPayService } from './http-pay.service';

describe('HttpPayService', () => {
  let service: HttpPayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpPayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
